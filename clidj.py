#!/usr/bin/python

from __future__ import print_function
import jack
import curses
import os
import time
import threading
import logging
from curses import COLOR_YELLOW,COLOR_WHITE,COLOR_BLUE,COLOR_GREEN,COLOR_BLACK,COLOR_RED,COLOR_MAGENTA,COLOR_CYAN
from unicurses import * 
from subprocess import call

path=[]
path.append("/tmp/m0")
path.append("/tmp/m1")
path.append('both')
path.append('shell')
path.append('')

file=[]
file.append("/home/dickreckard/P/clidj/log0")
file.append("/home/dickreckard/P/clidj/log1")

kkkk = open(r'/tmp/pycmd.log','w')
fnull= open(os.devnull, "w")

#os.mkfifo(path[0])
#os.mkfifo(path[1])

fifo=[]

loopstrob= False
timestrob= 0.1

loopflag= []
loopflag.append(False)
loopflag.append(False)

muteflag=[]
muteflag.append(0)
muteflag.append(0)

phoneflag=[]
phoneflag.append(0)
phoneflag.append(0)

looppos= []
looppos.append(0)
looppos.append(0)

loopdiff= []
loopdiff.append(0)
loopdiff.append(0)

loopWin=[]

cueWin=[]

cue = []
cue.append(0)
cue.append(0)

speedWin=[]

gainWin=[]

muteWin=[]

phoneWin=[]

speed= []
speed.append(1)
speed.append(1)

vol=[]
vol.append(63)
vol.append(63)

oldvol=[]
oldvol.append(63)
oldvol.append(63)

client = jack.Client("Cli")
client.midi_outports.register("midi_out")

slider= []
slider.append(0)
slider.append(0)
slider.append(0)


sslider= []
sslider.append(0)
sslider.append(0)

gslider= []
gslider.append(0)
gslider.append(0)

#HEIGHTS AND SIZES

FADER_BARS=30

FADER_TOP=10
FADER_LEFT=20

SPEED_STICK=18
GAIN_STICK=SPEED_STICK

centre=(FADER_BARS/2)+1;
centres=(SPEED_STICK/2);

msg='tzsk'

def windows():
# WINDOWS
        global crossfaderWin
        crossfaderWin  = curses.newwin(4,FADER_BARS+2,FADER_TOP,FADER_LEFT);
        crossfader(crossfaderWin)

        loopWin0=curses.newwin(4,7,FADER_TOP,FADER_LEFT-9)
        loopWin1=curses.newwin(4,7,FADER_TOP,FADER_LEFT+FADER_BARS+4)
        loopWin.append(loopWin0)
        loopWin.append(loopWin1)
        loopmain(loopWin)

        cueWin0=curses.newwin(3,FADER_BARS/2,FADER_TOP-4,FADER_LEFT-9)
        cueWin1=curses.newwin(3,FADER_BARS/2,FADER_TOP-4,FADER_LEFT+9+FADER_BARS/2)
        cueWin.append(cueWin0)
        cueWin.append(cueWin1)
        cuemain(cueWin)

        speedWin0=curses.newwin(SPEED_STICK,7,FADER_TOP+6,FADER_LEFT-9)
        speedWin1=curses.newwin(SPEED_STICK,7,FADER_TOP+6,FADER_LEFT+FADER_BARS+4)
        speedWin.append(speedWin0)
        speedWin.append(speedWin1)
        speedmain(0)
        speedmain(1)

        gainWin0=curses.newwin(GAIN_STICK,5,FADER_TOP+6,FADER_LEFT+4)
        gainWin1=curses.newwin(GAIN_STICK,5,FADER_TOP+6,FADER_LEFT+24)
        gainWin.append(gainWin0)
        gainWin.append(gainWin1)
        gainmain(0)
        gainmain(1)

        global timeWin
        timeWin=curses.newwin(4,6,20,5)
        timemain(timeWin)
	
	global msgWin
	msgWin=curses.newwin(5,60,50,5)
	msgmain(msg,2)

	muteWin0=curses.newwin(4,5,FADER_TOP+GAIN_STICK+6,FADER_LEFT+4)
	muteWin1=curses.newwin(4,5,FADER_TOP+GAIN_STICK+6,FADER_LEFT+24)
	muteWin.append(muteWin0)
        muteWin.append(muteWin1)
        mutemain(0)
        mutemain(1)

        phoneWin0=curses.newwin(4,5,FADER_TOP+GAIN_STICK+6,FADER_LEFT-4)
        phoneWin1=curses.newwin(4,5,FADER_TOP+GAIN_STICK+6,FADER_LEFT+32)
        phoneWin.append(phoneWin0)
        phoneWin.append(phoneWin1)
        phonemain(0)
        phonemain(1)

def rewindows():
        mutemain(0)
        mutemain(1)
	phonemain(0)
	phonemain(1)

        msgmain(msg,2)
        timemain(timeWin)
        gainmain(0)
        gainmain(1)
        speedmain(0)
        speedmain(1)
        cuemain(cueWin)
        loopmain(loopWin)
        crossfader(crossfaderWin)

	
def refreshall():
        wrefresh(crossfaderWin)
        wrefresh(loopWin[0])
        wrefresh(loopWin[1])
        wrefresh(cueWin[0])
        wrefresh(cueWin[1])
        wrefresh(speedWin[0])
        wrefresh(speedWin[1])
        wrefresh(gainWin[0])
        wrefresh(gainWin[1])
        wrefresh(timeWin)
 	wrefresh(msgWin)
	wrefresh(muteWin[0])
	wrefresh(muteWin[1])
	wrefresh(phoneWin[0])
	wrefresh(phoneWin[1])


def main(stdscr):
	global slider
        global loopstrob

	curses.noecho()
	curses.cbreak()
	stdscr.keypad(1)
	colors()
	
	curses.initscr()
        strobb=curses.newwin(0,0)
	windows()
	refreshall()

	fifo0= open(path[0], "w")
	fifo1 = open(path[1], "w")
	fifo.append(fifo0)
	fifo.append(fifo1)
	stdscr.refresh()


# MAIN LOOP AND KEYS
	while True:
    		c = stdscr.getch()

                if c == 27:
                        break  # Exit the while loop

		if c == ord('g'):
			curses.endwin
			windows()
			refreshall() # reset all windows


# CROSSFADER KEYS
# LEFT FADE
    		elif c == curses.KEY_LEFT:
        		if( slider[1] > (-FADER_BARS/2)) : 
				slider[1]-=1
			crossroutine(crossfaderWin)
                elif c == curses.KEY_SLEFT:
                        if( slider[1] > (-FADER_BARS/2)) :
                                slider[1]-=3
			crossroutine(crossfaderWin)

# RIGHT FADE
    		elif c == curses.KEY_RIGHT:
        		if( slider[1] < FADER_BARS/2-1):
				slider[1]+=1
                        crossroutine(crossfaderWin)
                elif c == curses.KEY_SRIGHT:
                        if( slider[1] < FADER_BARS/2-1):
                                slider[1]+=3
			crossroutine(crossfaderWin)
# ALL LEFT
		elif c == curses.KEY_DC :
			slider[1]=-FADER_BARS/2
                        crossroutine(crossfaderWin)
# ALL RIGHT
		elif c == curses.KEY_NPAGE :
			slider[1]= FADER_BARS/2-1
                        crossroutine(crossfaderWin)
# CENTER
		elif c == curses.KEY_END :
			slider[1]= 0
                        crossroutine(crossfaderWin)


# LOOPING KEYS
# LEFT LOOP	
		elif c == ord('C') :
			looproutine(0,stdscr)
#LEFT LOOP EARLIER
		elif c == ord('1') :
			looprout(0,0)
#LEFT LOOP LATER
		elif c == ord('2') :
			looprout(0,1)
#LEFT LOOP LONGER
                elif c == ord('3') :
                        looprout(0,2)
#LEFT LOOP SHORTER
                elif c == ord('4') :
                        looprout(0,3)


# RIGHT LOOP	
		elif c == ord('N') :
			looproutine(1,stdscr)
# RIGHT LOOP EARLIER
		elif c == ord('7'):
			looprout(1,0)
# RIGHT LOOP LATER
		elif c == ord('8'):
                        looprout(1,1)
# RIGHT LOOP LONGER
                elif c == ord('9'):
                        looprout(1,2)
# RIGHT LOOP SHORTER
                elif c == ord('0'):
                        looprout(1,3)
# CLEAR LEFT LOOP
		elif c == ord('c') :
			clearloop(0)

# CLEAR RIGHT LOOP
		elif c == ord('n') :
			clearloop(1)

# TIME
		elif c == ord(' ') :
			calctime(stdscr,timeWin)

# GAIN LEFT
# GAIN LEFTUP
		elif c == ord ('q'):
	                if (vol[0]==0):                           
                                mutemain(0)
			if (vol[0] < 100):
				vol[0]+=2
			gain(vol[0],0)
# GAIN LEFTDOWN
                elif c == ord ('a'):
                        if (vol[0] > 27):
                                vol[0]-=2
                        gain(vol[0],0)

# GAIN LETRESET
                elif c == ord ('Q'):
			if (vol[0]==0):                           
                                mutemain(0)
			vol[0]=63
                        gain(vol[0],0)

# GAIN LEFTMUTE
		elif c == ord ('z'):
                        if (muteflag[0]==0):
#                                call(["jack_disconnect", "jack_mixer:M0 Out L", "system:playback_3"],stdout = fnull, stderr = fnull)
#                                call(["jack_disconnect", "jack_mixer:M0 Out R", "system:playback_4"],stdout = fnull, stderr = fnull)
				muteon(0)
                                muteflag[0]=1
                        else:
#                                call(["jack_connect", "jack_mixer:M0 Out L", "system:playback_3"],stdout = fnull, stderr = fnull)
#                                call(["jack_connect", "jack_mixer:M0 Out R", "system:playback_4"],stdout = fnull, stderr = fnull)
                                muteflag[0]=0
                                mutemain(0)

#                        if (vol[0]==0):
#                                vol[0]=oldvol[0]
#				mutemain(0)
#			else:
#				oldvol[0]=vol[0]
#				vol[0]=0
#				muteon(0)
#			gain(vol[0],0)

#PHONES LEFT!
                elif c == ord ('`'):
                        if (phoneflag[0]==0):
                                try:
					call(["jack_connect", "jack_mixer:M0 Out L", "system:playback_1"],stdout = fnull, stderr = fnull)
                                	call(["jack_connect", "jack_mixer:M0 Out R", "system:playback_2"],stdout = fnull, stderr = fnull)
                                except:
				 	logging.exception('phones')
				phoneon(0)
                                phoneflag[0]=1
                        else:
                                phoneflag[0]=0
                                phonemain(0)



# GAIN IRGHTUP
                elif c == ord ('p'):
	                if (vol[1]==0):                           
                                mutemain(1)
                        if (vol[1] < 100):
                                vol[1]+=2
                        gain(vol[1],1)
# GAIN RIGHTDOWN
                elif c == ord ('l'):
                        if (vol[1] > 27):
                                vol[1]-=2
                        gain(vol[1],1)

# GAIN RIGHTRESET
                elif c == ord ('P'):
			if (vol[1]==0):
                        	mutemain(1)
                       	vol[1]=63
			gain(vol[1],1)


# GAIN RIGHTMUTE
		elif c == ord(','):
			if (muteflag[1]==0):
			        call(["jack_disconnect", "jack_mixer:M1 Out L", "system:playback_3"],stdout = fnull, stderr = fnull)
			        call(["jack_disconnect", "jack_mixer:M1 Out R", "system:playback_4"],stdout = fnull, stderr = fnull)
				muteon(1)
				muteflag[1]=1
			else:
				call(["jack_connect", "jack_mixer:M1 Out L", "system:playback_3"],stdout = fnull, stderr = fnull)
			        call(["jack_connect", "jack_mixer:M1 Out R", "system:playback_4"],stdout = fnull, stderr = fnull)
				muteflag[1]=0
				mutemain(1)

#			if (vol[1]==0):
#                                vol[1]=oldvol[1]
#                                mutemain(1)
#                        else:
#                                oldvol[1]=vol[1]
#                                vol[1]=0
#				muteon(1)
#
#			gain(vol[1],1)

#PHONES RIGHT!
                elif c == ord ('='):
                        if (phoneflag[1]==0):
                                try:
					call(["jack_connect", "jack_mixer:M1 Out L", "system:playback_1"],stdout = fnull, stderr = fnull)
                                	call(["jack_connect", "jack_mixer:M1 Out R", "system:playback_2"],stdout = fnull, stderr = fnull)
                                except:
					logging.exception('phonesright')
				phoneon(1)
                                phoneflag[1]=1
                        else:
                                phoneflag[1]=0
                                phonemain(1)




# CUEING KEYS
# SET LEFT CUE
		elif c == ord ('E'):
			cueset(0)

# SET RIGHT CUE
                elif c == ord ('I'):
                        cueset(1)

# LEFT CUE
                elif c == ord ('e'):
                        cuey(0)

# RIGHT CUE
                elif c == ord ('i'):
                        cuey(1)

# SPEED KEYS
# LEFT SPEED UP
                elif c == ord ('s'):
			speedroutine(0,0)
# LEFT SPEED DOWN
                elif c == ord ('x'):
			speedroutine(0,1)
# LEFT SPEED UP MORE
                elif c == ord ('S'):
                        speedroutine(0,3)
# LEFT SPEED DOWN MORE
                elif c == ord ('X'):
                        speedroutine(0,4)
# LEFT SPEED RESET
                elif c == ord ('w'):
			speedroutine(0,2)
# RIGHT SPEED UP
                elif c == ord ('k'):
			speedroutine(1,0)
# RIGHT SPEED DOWN
                elif c == ord ('m'):
			speedroutine(1,1)
# RIGHT SPEED RESET
                elif c == ord ('o'):
			speedroutine(1,2)
# RIGHTT SPEED UP MORE
                elif c == ord ('K'):
                        speedroutine(1,3)
# EIGHTT SPEED DOWN MORE
                elif c == ord ('M'):
                        speedroutine(1,4)


# MOVE LEFT
                elif c == 265:
			move(0,0)		
		elif c == 266:
			move(0,1)
		elif c == 267:
			move(0,2)
		elif c == 268:
			move(0,3)

# MOVE RIGHT
		elif c == 273:
			move(1,0)
		elif c == 274:
			move(1,1)
		elif c == 275:
			move(1,2)
		elif c == 276:
			move(1,3)

#PAUSE LEFT
		elif c == 92:
	        	fifo[0].write('pause\n')
        		fifo[0].flush()
	

#PAUSE RIGHT
		elif c == 47:
		        fifo[1].write('pause\n')
        		fifo[1].flush()

#STROBO
		elif c == ord("t"):
			strobo(strobb,loopstrob)
#		else:
#			print(c)




# CLOSING
	curses.nocbreak() 
	stdscr.keypad(0)
	curses.echo()
	curses.endwin()


def scale(val, src, dst):
    return ((val - src[0]) / (src[1]-src[0])) * (dst[1]-dst[0]) + dst[0]


def colors():
        curses.start_color()
        curses.init_pair(1, COLOR_WHITE, COLOR_BLUE);
        curses.init_pair(2, COLOR_BLACK, COLOR_WHITE);
        curses.init_pair(3, COLOR_BLACK, COLOR_GREEN);
        curses.init_pair(4, COLOR_RED, COLOR_WHITE);
        curses.init_pair(5, COLOR_BLACK, COLOR_CYAN);
        curses.init_pair(6, COLOR_BLACK, COLOR_MAGENTA);
        curses.init_pair(7, COLOR_WHITE, COLOR_CYAN);
        curses.init_pair(8, COLOR_WHITE, COLOR_MAGENTA);
        curses.init_pair(9, COLOR_YELLOW, COLOR_BLUE);
        curses.init_pair(10, COLOR_RED, COLOR_BLUE);
        curses.init_pair(11, COLOR_CYAN, COLOR_BLUE);
        curses.init_pair(12, COLOR_MAGENTA, COLOR_BLUE);
        curses.init_pair(13, COLOR_GREEN, COLOR_BLUE);
        curses.init_pair(14, COLOR_RED, COLOR_BLACK);
        curses.init_pair(15, COLOR_BLUE, COLOR_WHITE);
        curses.init_pair(16, COLOR_GREEN, COLOR_BLUE);
        curses.init_pair(17, COLOR_BLACK, COLOR_BLUE);

def msgmain(mssg,c):
	        wbkgd(msgWin,COLOR_PAIR(7)|A_BOLD);
                box(msgWin,0,0);
                wattrset(msgWin,COLOR_PAIR(7)|A_BOLD);
                mvwaddstr(msgWin,0,1,"what gets piped to "+path[c]);
                wattrset(msgWin,COLOR_PAIR(7)|A_BOLD);
                mvwaddstr(msgWin,1,2,mssg+'\n\n');
		wrefresh(msgWin)

def move(c,k):
	if (k==0):
		much=-30
	elif (k==1):
		much=-3
	elif (k==2):
		much=3
	elif (k==3):
       		much=30
	msg='seek '+str(much)+' 0\n'
	msgmain(msg,c)
	fifo[c].write(msg)
        fifo[c].flush()
	

def gain(vol,c):
	if (c==0):
		midi="11"
	if (c==1):
		midi="13"
	cmd="CTRL,1,"+midi+","+str(vol)
	msg="send_midi -J jack_mixer:midi in "+cmd
	msgmain(msg,3)
	call(["send_midi", "-J", "jack_mixer:midi in", cmd])
	gslider[c]=scale(vol,(27.0,100.0), ((GAIN_STICK/2),(-GAIN_STICK/2)))
	gainmain(c)

def gainmain(i):
                wbkgd(gainWin[i],COLOR_PAIR(1)|A_BOLD);
                box(gainWin[i],0,0);
                wattrset(gainWin[i],COLOR_PAIR(9)|A_BOLD);
                mvwaddstr(gainWin[i],0,1,"Gain");
                mvwaddstr(gainWin[i],1,0,"+");
                mvwaddstr(gainWin[i],GAIN_STICK-2,0,"-");
                mvwaddstr(gainWin[i],centres,0,"0");
                wattrset(gainWin[i],COLOR_PAIR(9));

                for j in range(1,GAIN_STICK-1):
                        mvwaddch(gainWin[i],j,2,'=');
                        wattrset(gainWin[i],COLOR_PAIR(1)|A_BOLD);
                gain_pos=int(gslider[i]);

                wattrset(gainWin[i],COLOR_PAIR(9)|A_BOLD);
                if (gain_pos == 0):
                        mvwaddch(gainWin[i],centres+gain_pos,2,ACS_DIAMOND)
                else:
                        mvwaddch(gainWin[i],centres+gain_pos,2,ACS_BLOCK)
		wrefresh(gainWin[i])

def calctime(s,timeWin):
	now = time.time()
        s.getch()
        future = time.time()
        timediff = future - now
	wbkgd(timeWin,COLOR_PAIR(1)|A_BOLD);
	mvwaddstr(timeWin,1,1,str("%.2f" % timediff))
	bpm=str(int(60/timediff))
	mvwaddstr(timeWin,2,1,bpm+' ')
	wrefresh(timeWin)

def timemain(timeWin):
                wbkgd(timeWin,COLOR_PAIR(1)|A_BOLD);
                box(timeWin,0,0);
                wattrset(timeWin,COLOR_PAIR(9)|A_BOLD);
		mvwaddstr(timeWin,0,1,"timah");
                wattrset(timeWin,COLOR_PAIR(9)|A_BOLD);
                mvwaddstr(timeWin,1,2,"0.00");

def speedroutine(c,o):
	if (o==0):
		if (speed[c] < 4 ):
			speed[c]+=0.005
	elif (o==1):
		if (speed[c] > 0.1):
			speed[c]-=0.005
	elif (o==3):
                if (speed[c] < 4 ):
                        speed[c]+=0.25
	elif (o==4):
                if (speed[c] > 0.1):
                        speed[c]-=0.25
	elif (o==2):
		speed[c]=1
	msg="speed_set "+str(speed[c])+"\n"	
	msgmain(msg,c)
	fifo[c].write(msg)
        fifo[c].flush()
	speedupdate(c)

def speedupdate(c):
	sslider[c]=scale(speed[c],(1.25,0.75), (-SPEED_STICK/2,SPEED_STICK/2))
	speedmain(c)

def speedmain(i):
	        wbkgd(speedWin[i],COLOR_PAIR(1)|A_BOLD);
       		box(speedWin[i],0,0);
        	wattrset(speedWin[i],COLOR_PAIR(9)|A_BOLD);
        	mvwaddstr(speedWin[i],0,1,"Pitch");
        	mvwaddstr(speedWin[i],1,0,"+");
        	mvwaddstr(speedWin[i],SPEED_STICK-2,0,"-");
        	mvwaddstr(speedWin[i],centres,0,"0");
        	wattrset(speedWin[i],COLOR_PAIR(9));

            	for j in range(1,SPEED_STICK-1):
			mvwaddch(speedWin[i],j,3,ACS_S1);
        		wattrset(speedWin[i],COLOR_PAIR(1)|A_BOLD);
        	speed_pos=int(sslider[i]);

        	wattrset(speedWin[i],COLOR_PAIR(9)|A_BOLD);
        	if (speed_pos == 0):
                	mvwaddch(speedWin[i],centres+speed_pos,4,ACS_DIAMOND)
                	mvwaddch(speedWin[i],centres+speed_pos,2,ACS_DIAMOND)
                	mvwaddch(speedWin[i],centres+speed_pos,3,ACS_DIAMOND)
        	else:
            		mvwaddch(speedWin[i],centres+speed_pos,3,ACS_DIAMOND)

		wrefresh(speedWin[i])	

class strob(threading.Thread):
   def __init__ (self,stt):
        threading.Thread.__init__ (self)
        self.ss=stt
	global loopstrob
	global stopstrob 
  
   def run(self):
        while (loopstrob==True):
                wbkgd(self.ss,COLOR_PAIR(1)|A_BOLD);
                wrefresh(self.ss)
#		curses.endwin()
#               windows()
#	        refreshall()
                time.sleep(timestrob)
                wbkgd(self.ss,COLOR_PAIR(2)|A_BOLD);
                wrefresh(self.ss)
#               windows()
#               refreshall()
		time.sleep(timestrob)
	curses.endwin()
	windows()
	refreshall()
	#   except:
	#	break

def strobo(strr,loo):
	global loopstrob
	if (loopstrob == True):
		loopstrob = False
	else: 
		loopstrob = True
		Strob = strob(strr)
#		Strob.setDaemon(True)
		Strob.start()
		
def looproutine(c,s):
	loopflag[c]=False
	fifo[c].write('get_time_pos\n')
	fifo[c].flush()
	now = time.time()
	time.sleep(0.1)
	fil = open(file[c], 'r')
	last=([i for i in fil.read().split('\n') if 'POSITION' in i][-1])
	looppos[c]= last.split('=')[1]
	s.getch()
	future = time.time()
	loopdiff[c] = future - now
	startloop(c,looppos[c],loopdiff[c])

def startloop(c,pos,diff):
	loopflag[c]=True
	Loop = loopz(c, pos, diff)
	Loop.start()
        wattrset(loopWin[c],COLOR_PAIR(14)|A_BOLD);
	wbkgd(loopWin[c],COLOR_PAIR(14)|A_BOLD);
	mvwaddstr(loopWin[c],1,2,"ON ");
        mvwaddstr(loopWin[c],2,1,str("%.2f" % loopdiff[c]));
	wrefresh(loopWin[c])

def looprout(c,v):
	if v==0:
		looppos[c]=float(looppos[c])-0.025
	elif v==1:
		looppos[c]=float(looppos[c])+0.025
	elif v==2:
		loopdiff[c]=float(loopdiff[c])-0.025
	        mvwaddstr(loopWin[c],2,1,str("%.2f" % loopdiff[c]));
		wrefresh(loopWin[c])	
	elif v==3:
		loopdiff[c]=float(loopdiff[c])+0.025
        	mvwaddstr(loopWin[c],2,1,str("%.2f" % loopdiff[c]));
		wrefresh(loopWin[c])

class loopz(threading.Thread):
    def __init__ (self, chan, posi, diffe):
        self.c = chan
	self.pos = posi
	self.diff= diffe
        threading.Thread.__init__ (self)
   
    def run(self):
	while (loopflag[self.c]==True):
	   try:
		pos=str(looppos[self.c])
		diff=loopdiff[self.c]
		msg='seek '+pos+' 2\n'
		msgmain(msg,self.c)
		fifo[self.c].write('seek '+pos+' 2\n')
		fifo[self.c].flush()
                time.sleep(diff)
	   except:
#		loopflag[self.c]==False
		break

def clearloop(c):
	loopflag[c]=False
	wbkgd(loopWin[c],COLOR_PAIR(1)|A_BOLD);
	wattrset(loopWin[c],COLOR_PAIR(9)|A_BOLD);
	mvwaddstr(loopWin[c],1,2,"   ");	
 	mvwaddstr(loopWin[c],2,2,"OFF");	
	wrefresh(loopWin[c])

def loopmain(loopWin):
	for i in range (0,2):
        	wbkgd(loopWin[i],COLOR_PAIR(1)|A_BOLD);
		box(loopWin[i],0,0);
		wattrset(loopWin[i],COLOR_PAIR(9)|A_BOLD);
		mvwaddstr(loopWin[i],0,1,"Loopda");
        	mvwaddstr(loopWin[i],3,1,"Loop"+str(i));
        	wattrset(loopWin[i],COLOR_PAIR(9)|A_BOLD);
		mvwaddstr(loopWin[i],2,2,"OFF");

def mutemain(c):
	
	call(["jack_connect", "jack_mixer:M"+str(c)+" Out L", "system:playback_3"], stdout = fnull, stderr = fnull)
	call(["jack_connect", "jack_mixer:M"+str(c)+" Out R", "system:playback_4"], stdout = fnull, stderr = fnull)
        wbkgd(muteWin[c],COLOR_PAIR(14)|A_BOLD);
	box(muteWin[c],0,0)
	wattrset(muteWin[c],COLOR_PAIR(9)|A_BOLD);
        mvwaddstr(muteWin[c],0,1,"Mute");
	mvwaddstr(muteWin[c],1,1,"   ");
        mvwaddstr(muteWin[c],2,1,"OFF");
        wrefresh(muteWin[c])

def muteon(c):
        call(["jack_disconnect", "jack_mixer:M"+str(c)+" Out L", "system:playback_3"], stdout = fnull, stderr = fnull)
        call(["jack_disconnect", "jack_mixer:M"+str(c)+" Out R", "system:playback_4"], stdout = fnull, stderr = fnull)
	wattrset(muteWin[c],COLOR_PAIR(14)|A_BOLD);
        wbkgd(muteWin[c],COLOR_PAIR(14)|A_BOLD);
        mvwaddstr(muteWin[c],1,1,"ON ");
        mvwaddstr(muteWin[c],2,1,"   ");
        wrefresh(muteWin[c])
	
def phonemain(c):
        try:
		call(["jack_disconnect", "jack_mixer:M"+str(c)+" Out L", "system:playback_1"], stdout = fnull, stderr = fnull)
        	call(["jack_disconnect", "jack_mixer:M"+str(c)+" Out R", "system:playback_2"], stdout = fnull, stderr = fnull)
        except:
                return_code = kkkk	
	wbkgd(phoneWin[c],COLOR_PAIR(1)|A_BOLD);
        box(phoneWin[c],0,0)
        wattrset(phoneWin[c],COLOR_PAIR(9)|A_BOLD);
        mvwaddstr(phoneWin[c],0,1,"Monit");
        mvwaddstr(phoneWin[c],1,1,"   ");
        mvwaddstr(phoneWin[c],2,1,"OFF");
        wrefresh(phoneWin[c])

def phoneon(c):
        wattrset(phoneWin[c],COLOR_PAIR(14)|A_BOLD);
        wbkgd(phoneWin[c],COLOR_PAIR(14)|A_BOLD);
        mvwaddstr(phoneWin[c],1,1,"ON ");
        mvwaddstr(phoneWin[c],2,1,"   ");
        wrefresh(phoneWin[c])

def cuemain(cueWin):
        for i in range (0,2):
                wbkgd(cueWin[i],COLOR_PAIR(1)|A_BOLD);
                box(cueWin[i],0,0);
                wattrset(cueWin[i],COLOR_PAIR(1)|A_BOLD);
                mvwaddstr(cueWin[i],0,1,"Cu-e");
                mvwaddstr(cueWin[i],2,1,"CH"+str(i));
                wattrset(cueWin[i],COLOR_PAIR(9)|A_BOLD);
                mvwaddstr(cueWin[i],1,2,"None");

def cueset(c):
        fifo[c].write('get_time_pos\n')
        fifo[c].flush()
        time.sleep(0.1)
        fil = open(file[c], 'r')
        last=([i for i in fil.read().split('\n') if 'POSITION' in i][-1])
        post= last.split('=')[1]
	cue[c]=post
	post=int(float(post))
	m, s = divmod(post, 60)
	post= "%02d:%02d" % (m, s)
	mvwaddstr(cueWin[c],1,2,post);
	wrefresh(cueWin[c])

def cuey(c):
	msg='seek '+str(cue[c])+' 2\n'
	msgmain(msg,c)
        fifo[c].write('seek '+str(cue[c])+' 2\n')
        fifo[c].flush()


def crossroutine(crossfaderWin):
        if (slider[1] < 0):
                fad1=100
                fad2=100+scale(slider[1], (-FADER_BARS/2.0,0.0), (-100.0, 0.0))
        elif (slider[1] > 0):
                fad1=100+scale(slider[1], (FADER_BARS/2.0-1,0.0), (-100.0,0.0))
                fad2=100
        else:
                fad1=100
                fad2=100
	msg1="volume "+str(int(fad1))+"% 1\n"
        msg2="volume "+str(int(fad2))+"% 1\n"
	msgmain(msg1+msg2,2)
	fifo[0].write(msg1)
        fifo[0].flush()
        fifo[1].write(msg2)
        fifo[1].flush()

#       call(["send_midi", "-J","jack_mixer:midi in","CTRL,1,11,"+str(int(fad1)),"CTRL,1,13,"+str(int(fad2))])

	crossfader(crossfaderWin)



def crossfader(crossfaderWin):
	
   try:
	wbkgd(crossfaderWin,COLOR_PAIR(1)|A_BOLD);
	box(crossfaderWin,0,0);
	wattrset(crossfaderWin,COLOR_PAIR(10)|A_BOLD);
	wattrset(crossfaderWin,COLOR_PAIR(9)|A_BOLD);
	mvwaddstr(crossfaderWin,0,1,"Braaaaaaaaaa");
    	mvwaddstr(crossfaderWin,3,1,"CH0");
    	mvwaddstr(crossfaderWin,3,FADER_BARS-2,"CH1");
    	mvwaddstr(crossfaderWin,3,centre,"|");
	wattrset(crossfaderWin,COLOR_PAIR(9)|A_BOLD);
	
	for j in range(0,  FADER_BARS):
            mvwaddch(crossfaderWin,1,j+1,ACS_BULLET);
	wattrset(crossfaderWin,COLOR_PAIR(1)|A_BOLD);        
	fader_pos=slider[1];	

	wattrset(crossfaderWin,COLOR_PAIR(1)|A_BOLD);	
	if (fader_pos == 0):
		mvwaddch(crossfaderWin,1,centre+fader_pos,ACS_BLOCK)
            	mvwaddch(crossfaderWin,1,centre+fader_pos-1,' ')
            	mvwaddch(crossfaderWin,1,centre+fader_pos+1,' ')
	elif (fader_pos == -(FADER_BARS/2)):
            mvwaddch(crossfaderWin,1,centre+fader_pos,ACS_BLOCK)
            mvwaddch(crossfaderWin,1,centre+fader_pos+1,ACS_LTEE)
	elif (fader_pos == FADER_BARS/2-1):
            mvwaddch(crossfaderWin,1,centre+fader_pos,ACS_BLOCK)
            mvwaddch(crossfaderWin,1,centre+fader_pos-1,ACS_RTEE)
 	else:
            mvwaddch(crossfaderWin,1,centre+fader_pos,ACS_BLOCK)
            mvwaddch(crossfaderWin,1,centre+fader_pos-1,ACS_RTEE)
            mvwaddch(crossfaderWin,1,centre+fader_pos+1,ACS_LTEE)


	fader_pos=slider[0];
	wattrset(crossfaderWin,COLOR_PAIR(1)|A_BOLD);
	wrefresh(crossfaderWin)
   except:
	logging.exception()

#curses.wrapper(main)

try:
    curses.wrapper(main)
except:
    loopflag[0]=False
    loopflag[1]=False
    loopstrob=False
    time.sleep(1)
    curses.nocbreak()
#    stdscr.keypad(0)
    curses.echo()
    curses.endwin()








